﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;

public class RecordedPointMan : MonoBehaviour {

    public GameObject SpineBase;
    public GameObject SpineMid;
    public GameObject Neck;
    public GameObject Head;
    public GameObject ShoulderLeft;
    public GameObject ElbowLeft;
    public GameObject WristLeft;
    public GameObject HandLeft;
    public GameObject ShoulderRight;
    public GameObject ElbowRight;
    public GameObject WristRight;
    public GameObject HandRight;
    public GameObject HipLeft;
    public GameObject KneeLeft;
    public GameObject AnkleLeft;
    public GameObject FootLeft;
    public GameObject HipRight;
    public GameObject KneeRight;
    public GameObject AnkleRight;
    public GameObject FootRight;
    public GameObject SpineShoulder;
    public GameObject HandTipLeft;
    public GameObject ThumbLeft;
    public GameObject HandTipRight;
    public GameObject ThumbRight;

    private GameObject[] _bones; //internal handle for the bones of the model
    //private Vector4[] _bonePos; //internal handle for the bone positions from the kinect

    public int player = -1;

    public float scale = 1.0f;
    float timer = 0f;

    private Dictionary<JointType, GameObject> _BoneMap;
    private BodySourceManager bodySourceManager;
    RecordingController recordingController;
    LineRendererController lrc;
    public Vector3 offset;
    void Start()
    {
        bodySourceManager = GameObject.FindObjectOfType<BodySourceManager>();
        lrc = this.GetComponent<LineRendererController>();
        recordingController = RecordingController.Instance;
        //store bones in a list for easier access
       _BoneMap = new Dictionary<JointType, GameObject>()
        {
            { JointType.SpineBase, SpineBase},
	        { JointType.SpineMid, SpineMid},
            { JointType.Neck, Neck},
	        { JointType.Head, Head},
	        { JointType.ShoulderLeft, ShoulderLeft},
	        { JointType.ElbowLeft, ElbowLeft},
	        { JointType.WristLeft, WristLeft},
	        { JointType.HandLeft, HandLeft},
	        { JointType.ShoulderRight, ShoulderRight},
	        { JointType.ElbowRight, ElbowRight},
	        { JointType.WristRight, WristRight},
	        { JointType.HandRight, HandRight},
	        { JointType.HipLeft, HipLeft},
	        { JointType.KneeLeft, KneeLeft},
	        { JointType.AnkleLeft, AnkleLeft},
	        { JointType.FootLeft, FootLeft},
	        { JointType.HipRight, HipRight},
	        { JointType.KneeRight, KneeRight},
	        { JointType.AnkleRight, AnkleRight},
	        { JointType.FootRight, FootRight},
            { JointType.SpineShoulder, SpineShoulder},
            { JointType.HandTipLeft, HandTipLeft},
            { JointType.ThumbLeft, ThumbLeft},
            { JointType.HandTipRight, HandTipRight},
            { JointType.ThumbRight, ThumbRight}
        };
        //_bonePos = new Vector4[(int)BoneIndex.Num_Bones];

    }

    public float lerpSpeed = 30f;
    Vector3 noPlayerPosition = new Vector3(0, 0, 1000);

    public void SetFrame(int frame)
    {
        if (recordingController.recordedClip.Count == 0)
        {
            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++)
                _BoneMap[jt].transform.localPosition = noPlayerPosition;
            lrc.RefreshPoints();
            player = -1;
            return;
        }
        Frame skeletonFrame = recordingController.recordedClip[frame];
        if (skeletonFrame == null)
            return;
        float currentAngle = (float)bodySourceManager.tiltRadians;
        float cos = Mathf.Cos(currentAngle);
        float sin = Mathf.Sin(currentAngle);

        CameraSpacePoint[] points = new CameraSpacePoint[skeletonFrame.bones.Length];
        int j = 0;
        ColorSpacePoint[] cPoints = new ColorSpacePoint[skeletonFrame.bones.Length];

        for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++, j++)
        {
            MyVector pos = skeletonFrame.bones[j].Clone();
            float newY = pos.y * cos - pos.z * sin;
            float newZ = pos.z * cos + pos.y * sin;
            pos.y = newY - offset.y;
            pos.z = newZ - offset.z;
            points[j] = Frame.MyVectorToCameraSpacePoint(pos);
        }
        KinectSensor.GetDefault().CoordinateMapper.MapCameraPointsToColorSpace(points, cPoints);
        j = 0;

        for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++, j++)
        {
            Vector3 pos = Frame.ColorSpacePointToVector3(cPoints[j]);
            pos.x -= 960;
            pos.y = 540 - pos.y;

            if (_BoneMap[jt] != null && pos.y != Mathf.Infinity)
                _BoneMap[jt].transform.localPosition = Vector3.Lerp(_BoneMap[jt].transform.localPosition, pos, Time.deltaTime * lerpSpeed);
        }
        lrc.RefreshPoints();
    }

    public void SetFrame(Frame frame)
    {
        if (frame == null)
        {
            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++)
                _BoneMap[jt].transform.localPosition = noPlayerPosition;
            lrc.RefreshPoints();
            player = -1;
            return;
        }

        float currentAngle = (float)bodySourceManager.tiltRadians;
        float cos = Mathf.Cos(currentAngle);
        float sin = Mathf.Sin(currentAngle);

        CameraSpacePoint[] points = new CameraSpacePoint[frame.bones.Length];
        int j = 0;
        ColorSpacePoint[] cPoints = new ColorSpacePoint[frame.bones.Length];

        for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++, j++)
        {
            MyVector pos = frame.bones[j].Clone();
            float newY = pos.y * cos - pos.z * sin;
            float newZ = pos.z * cos + pos.y * sin;
            pos.y = newY - offset.y;
            pos.z = newZ - offset.z;
            pos.x -= offset.x;
            points[j] = Frame.MyVectorToCameraSpacePoint(pos);
        }
        KinectSensor.GetDefault().CoordinateMapper.MapCameraPointsToColorSpace(points, cPoints);
        j = 0;
        for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++, j++)
        {
            Vector3 pos = Frame.ColorSpacePointToVector3(cPoints[j]);
            pos.x -= 960;
            pos.y = 540 - pos.y;

            if (_BoneMap[jt] != null && pos.y != Mathf.Infinity)
                _BoneMap[jt].transform.localPosition = Vector3.Lerp(_BoneMap[jt].transform.localPosition, pos, Time.deltaTime * lerpSpeed);
        }

        lrc.RefreshPoints();
    }

}
