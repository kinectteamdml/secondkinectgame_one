/*
 * KinectModelController.cs - Moves every 'bone' given to match
 * 				the position of the corresponding bone given by
 * 				the kinect. Useful for viewing the point tracking
 * 				in 3D.
 * 
 * 		Developed by Peter Kinney -- 6/30/2011
 * 
 */

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;

public class KinectPointController : MonoBehaviour {
	
	public GameObject SpineBase;
	public GameObject SpineMid;
	public GameObject Neck;
	public GameObject Head;
	public GameObject ShoulderLeft;
	public GameObject ElbowLeft;
	public GameObject WristLeft;
	public GameObject HandLeft;
	public GameObject ShoulderRight;
	public GameObject ElbowRight;
	public GameObject WristRight;
	public GameObject HandRight;
	public GameObject HipLeft;
	public GameObject KneeLeft;
	public GameObject AnkleLeft;
	public GameObject FootLeft;
	public GameObject HipRight;
	public GameObject KneeRight;
	public GameObject AnkleRight;
	public GameObject FootRight;
    public GameObject SpineShoulder;
    public GameObject HandTipLeft;
    public GameObject ThumbLeft;
    public GameObject HandTipRight;
    public GameObject ThumbRight;

	
	private GameObject[] _bones; //internal handle for the bones of the model
	//private Vector4[] _bonePos; //internal handle for the bone positions from the kinect
    private Dictionary<JointType, GameObject> _BoneMap;
	public int player;
	
	public float scale = 1.0f;
    LineRendererController lrc;
    BodySourceManager bodySourceManager;

    public float lerpSpeed = 10f;
    public Vector3 LegsPosition;
    void Start () {
        lrc = GetComponent<LineRendererController>();
        bodySourceManager = GameObject.FindObjectOfType<BodySourceManager>();
		//_bonePos = new Vector4[(int)BoneIndex.Num_Bones];
        _BoneMap = new Dictionary<JointType, GameObject>()
        {
            { JointType.SpineBase, SpineBase},
	        { JointType.SpineMid, SpineMid},
            { JointType.Neck, Neck},
	        { JointType.Head, Head},
	        { JointType.ShoulderLeft, ShoulderLeft},
	        { JointType.ElbowLeft, ElbowLeft},
	        { JointType.WristLeft, WristLeft},
	        { JointType.HandLeft, HandLeft},
	        { JointType.ShoulderRight, ShoulderRight},
	        { JointType.ElbowRight, ElbowRight},
	        { JointType.WristRight, WristRight},
	        { JointType.HandRight, HandRight},
	        { JointType.HipLeft, HipLeft},
	        { JointType.KneeLeft, KneeLeft},
	        { JointType.AnkleLeft, AnkleLeft},
	        { JointType.FootLeft, FootLeft},
	        { JointType.HipRight, HipRight},
	        { JointType.KneeRight, KneeRight},
	        { JointType.AnkleRight, AnkleRight},
	        { JointType.FootRight, FootRight},
            { JointType.SpineShoulder, SpineShoulder},
            { JointType.HandTipLeft, HandTipLeft},
            { JointType.ThumbLeft, ThumbLeft},
            { JointType.HandTipRight, HandTipRight},
            { JointType.ThumbRight, ThumbRight}
        };
	}

    Vector3 startPosition = new Vector3(0, 0, 1000);

    void Update()
    {
       
        Body body = GetTrackedBody();
        if (body != null)
        {
          /*  float currentAngle = (float)bodySourceManager.tiltRadians;
            float cos = Mathf.Cos(currentAngle);
            float sin = Mathf.Sin(currentAngle);*/
            CameraSpacePoint[] points = new CameraSpacePoint[body.Joints.Count];
            int j = 0;
            ColorSpacePoint[] cPoints = new ColorSpacePoint[body.Joints.Count];
            LegsPosition = (Frame.CameraSpacePointToVector3(body.Joints[JointType.FootLeft].Position) + Frame.CameraSpacePointToVector3(body.Joints[JointType.FootRight].Position))/2f;
            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++, j++)
            {
                
                points[j] = body.Joints[jt].Position;
            }
            KinectSensor.GetDefault().CoordinateMapper.MapCameraPointsToColorSpace(points,cPoints);
            j = 0;
            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++, j++)
            {
                //Vector3 pos = Frame.CameraSpacePointToVector3(body.Joints[jt].Position);
                Vector3 pos = Frame.ColorSpacePointToVector3(cPoints[j]);
                pos.x -= 960;
                pos.y = 540 - pos.y;
              /*  float newY = pos.y * cos + pos.z * sin;
                float newZ = pos.z * cos - pos.y * sin;
                pos.y = newY;
                pos.z = newZ;*/
                if (pos.y!=Mathf.Infinity)
                    _BoneMap[jt].transform.localPosition = Vector3.Lerp(_BoneMap[jt].transform.localPosition, pos, lerpSpeed * Time.deltaTime);
            }
        }
        else
        {
            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++)
            {
                _BoneMap[jt].transform.localPosition = startPosition;
            }
        }
        lrc.RefreshPoints();
	}

    public Body GetTrackedBody()
    {
        Body[] bodies = bodySourceManager.GetData();
        if (bodies == null)
            return null;
        player = -1;
        foreach (Body b in bodySourceManager.GetData())
        {
            if (b.IsTracked)
            {
                player = 1;
                return b;
            }
        }
        return null;
    }

}
