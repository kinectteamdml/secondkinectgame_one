﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Windows.Kinect;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
public class RecordingController : MonoBehaviour {
    private static RecordingController instance_;
    public static RecordingController Instance
    {
        get {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<RecordingController>();
            return instance_;
        }
    }

    public List<Frame> recordedClip;
    bool recording = false;
    public VideoCutSlider vcs;
    public RecordedPointMan rpm;
    bool play = false;
    private BodySourceManager bodySourceManager;
    public void Start()
    {
        bodySourceManager = GameObject.FindObjectOfType<BodySourceManager>();
    }

    public GameObject quitDialog;
	void Update () {
        if (Input.GetKey(KeyCode.Escape))
            quitDialog.SetActive(true);

        if (recording)
        {
            Body body = GetTrackedBody();
            if (body!=null)
            {
                recordedClip.Add(new Frame(bodySourceManager.tiltRadians, body, Time.deltaTime));
                vcs.SetRange(0, recordedClip.Count - 1);
            }
           
        }
	}

    public void Quit()
    {
        Application.Quit();
    }

    public Body GetTrackedBody()
    {
        Body[] bodies = bodySourceManager.GetData();
        if (bodies == null)
            return null;

        foreach (Body b in bodySourceManager.GetData())
        {
            if (b.IsTracked)
                return b;
        }
        return null;
    }

    public void SetFrame()
    {
        rpm.SetFrame((int)vcs.current);
    }
    public void StartRecording()
    {
        recordedClip = new List<Frame>();
        recording = true;
        rpm.player = -1;
    }

    public void StopRecording()
    {
        recording = false;
    }

    public Frame[] GetRecordedFrames()
    {        
        return recordedClip.ToArray();
    }

    public void SaveRecord(string name)
    {
        string filePath = Application.dataPath + "/../Records/" + name;
        FileStream output = new FileStream(filePath, FileMode.Create);
        BinaryFormatter bf = new BinaryFormatter();
        Frame[] data = new Frame[vcs.end - vcs.start];
        for (int ii = (int)vcs.start; ii < (int)vcs.end; ii++)
            data[ii - vcs.start] = recordedClip[ii];
        bf.Serialize(output, data);
        output.Close();
        Debug.Log("Saved");
    }

    public void SaveRecord(Text t)
    {
        SaveRecord(t.text);
    }
}
